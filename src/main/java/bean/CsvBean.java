package bean;

import com.opencsv.bean.CsvBindByName;

public class CsvBean {

    @CsvBindByName(column = "Index")
    private Integer index;

    @CsvBindByName(column = "Year")
    private Integer year;

    @CsvBindByName(column = "Age")
    private Integer age;

    @CsvBindByName(column = "Name")
    private String name;

    @CsvBindByName(column = "Movie")
    private String movie;

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMovie() {
        return movie;
    }

    public void setMovie(String movie) {
        this.movie = movie;
    }
}
