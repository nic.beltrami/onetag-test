import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import service.WinnerService;
import service.WinnerServiceImpl;

import java.io.FileNotFoundException;

public class Application {

    private static Logger logger = LoggerFactory.getLogger(Application.class);

    /*
    Scrivi un programma che legga i due dataset in CSV in allegato,
    contenenti i record dei vincitori dei premi Oscar, stampi i nomi degli attori/attrici che ne hanno vinti più di uno,
    in ordine decrescente di premi vinti.

    A parità di premi, stampa prima gli attori anagraficamente più giovani.
     */
    public static void main(String args[]) throws FileNotFoundException {

        WinnerService winnerService = new WinnerServiceImpl();
        winnerService.findBetterWinners().forEach(mostImportantWinner -> logger.info(" Name: " + mostImportantWinner.getName() + " - Born: " + mostImportantWinner.getBirthYear() + " - Oscar: " + mostImportantWinner.getCount()));

    }
}
