package mapper;

import bean.CsvBean;
import bean.WinnerBean;

import java.util.List;
import java.util.Map;

public class WinnerMapper {

    public WinnerBean toWinnerBean(Map.Entry<String, List<CsvBean>> winner) {
        WinnerBean winnerBean = new WinnerBean();
        winnerBean.setName(winner.getKey());
        winnerBean.setCount(winner.getValue().size());
        Integer birthYear = winner.getValue().get(0).getYear() - winner.getValue().get(0).getAge();
        winnerBean.setBirthYear(birthYear);
        return winnerBean;
    }
}
