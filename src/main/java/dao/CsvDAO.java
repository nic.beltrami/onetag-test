package dao;

import bean.CsvBean;

import java.io.FileNotFoundException;
import java.util.List;

public interface CsvDAO {

    List<CsvBean> readCSV(final String fileName) throws FileNotFoundException;

}
