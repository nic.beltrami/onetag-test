package dao;

import bean.CsvBean;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.Reader;
import java.util.List;

public class CsvDAOImpl implements CsvDAO {

    @Override
    public List<CsvBean> readCSV(final String fileName) throws FileNotFoundException {

        Reader reader = new BufferedReader(new FileReader(ClassLoader.getSystemResource(fileName).getPath()));

        CsvToBean<CsvBean> csvReader = new CsvToBeanBuilder(reader)
                .withType(CsvBean.class)
                .withSeparator(',')
                .withIgnoreLeadingWhiteSpace(true)
                .withIgnoreEmptyLine(true)
                .build();

        return csvReader.parse();
    }
}
