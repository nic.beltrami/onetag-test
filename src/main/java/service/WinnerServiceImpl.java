package service;

import bean.CsvBean;
import bean.WinnerBean;
import dao.CsvDAO;
import dao.CsvDAOImpl;
import mapper.WinnerMapper;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class WinnerServiceImpl implements WinnerService {

    private CsvDAO csvDAO;

    private WinnerMapper winnerMapper;

    public WinnerServiceImpl() {
        this.csvDAO = new CsvDAOImpl();
        this.winnerMapper = new WinnerMapper();
    }

    @Override
    public List<WinnerBean> findBetterWinners() throws FileNotFoundException {

        List<CsvBean> winners = new ArrayList<>();

        winners.addAll(csvDAO.readCSV("oscar_age_male.csv"));
        winners.addAll(csvDAO.readCSV("oscar_age_female.csv"));

        Map<String, List<CsvBean>> mostImportantWinnersMap = winners.stream()
                .collect(Collectors.groupingBy(CsvBean::getName))
                .entrySet().stream()
                .filter(winner -> winner.getValue().size() > 1)
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

        final List<WinnerBean> mostImportantWinners = new ArrayList<>();
        mostImportantWinnersMap.entrySet().forEach(winner -> {
            if (winner.getValue().stream().map(csvBean -> csvBean.getYear() - csvBean.getAge()).distinct().count() > 1) {
                throw new IllegalStateException("Birth Year is not match " + winner.getKey());
            }
            mostImportantWinners.add(this.winnerMapper.toWinnerBean(winner));
        });

        return mostImportantWinners.stream()
                .sorted(
                    Comparator.comparing(WinnerBean::getCount)
                        .reversed()
                        .thenComparing(WinnerBean::getBirthYear, Comparator.reverseOrder())
                )
                .collect(Collectors.toList());

    }
}
