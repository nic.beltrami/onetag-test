package service;

import bean.WinnerBean;

import java.io.FileNotFoundException;
import java.util.List;

public interface WinnerService {

    List<WinnerBean> findBetterWinners() throws FileNotFoundException;

}
