import bean.WinnerBean;
import org.junit.jupiter.api.Test;
import service.WinnerService;
import service.WinnerServiceImpl;

import java.io.FileNotFoundException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class ApplicationTest {

    private WinnerService winnerService;

    public ApplicationTest() {
        this.winnerService = new WinnerServiceImpl();
    }

    @Test
    void findBetterWinnersTest() throws FileNotFoundException {
        List<WinnerBean> winners = this.winnerService.findBetterWinners();

        assertTrue(winners.size() == 23);

        // Check first winner
        assertTrue(winners.get(0).getName().equals("Katharine Hepburn"));
        assertTrue(winners.get(0).getCount().equals(4));
        assertTrue(winners.get(0).getBirthYear().equals(1907));

        // Check third winner
        assertTrue(winners.get(2).getName().equals("Hilary Swank"));
        assertTrue(winners.get(2).getCount().equals(2));
        assertTrue(winners.get(2).getBirthYear().equals(1975));

        // Check last winner
        assertTrue(winners.get(winners.size() - 1).getName().equals("Fredric March"));
        assertTrue(winners.get(winners.size() - 1).getCount().equals(2));
        assertTrue(winners.get(winners.size() - 1).getBirthYear().equals(1898));

    }
}
